# Role selection for Global Gathering Working Group

## NEED FOR WORKING GROUP FOR GOSH GATHERING

- JS : a person who organized one before (continue knowledge)
- LO: How would the working group arise? How to set it's identity? E.g. do the wider community or council define lines of work for the group?
- EB: Pass.
-AQ: committment for the year
- EB: What about having one council member as the main person who leads on forming the group
  - The council can revoke it
  - The council and the group would each have a person that faces the other
- AQ: Or a rotating liason council person to work with the working group?
  - yes can create a double-link, one link to face the WG, one link to face back to the Council
- MF: From last time it was mostly self-organising, if so the council would just allow that to form organically as long as they "report back" to the council
- JS: E.g. a call for hosts, then there needs to be some way to decide who hosts, which is tied into the diversity of GOSH
- EB: The internal coordinator as "insurance" in the working group from the council can step in in case of problems, while the group can form organically
- AQ: And we haven't really decide what the council's role will be
  - Can be very hands off or hands on

## DECISIONS:

- If and we want this gathering?(I am not sure IF is applicable as we are contracted to with Sloan!)
  - *+1 (Overided +7 cuz we have to! )*
- when do we want this gathering?

## SYNTHESIZED NEEDS STATEMENT: 
**DRAFT:** The Council needs to delegate authority to a GOSH member to anchor the new GOSH Working Group, create an open call for membership, build membership, and then choose another WG member to report back to the Council. A Council person will be point on checking-in with prior successful organizers of GOSH Global Gathering to be the anchor. 
  - *+1 (all 6, still ask pierre!)*


We can test our role nomination process to decide who contacts Julieta and Greg to see if they're up for starting the GOSH Global working group.

## APPROVE NEEDS STATEMENT (ASK FOR THUMBS UP)

**POSTPONE TO NEXT WEEK: WHAT ARE ELEMENTS OF SCOPE OF WORKING GROUP THAT WOULD LEAD TO SUCCESS**
- AQ: What are the needs at the Physical Location? E.g. housing needs? Cool animals? Beaches? Workbenches?
- EB: build membership in the working group
- JS: Definitely need local person, which means group needs to decide location first, too.
- MF: The group should be as diverse as possible so that more things will be considered e.g. edge case.
- HPY: want a diverse group, what does diverse mean?

## SYNTHESIZE THE SCOPE FOR HELPING CIRCLE: 

## APPROVE SCOPE

SCOPE of the role: 

- AQ
- JS
- MF

**SYNTHESIZED SCOPE:** Contact experienced organizers Greg and Julieta and work with together to draft a scope for what this WG should do, i.e. contact hopeful future location hosts, set date, all logistics. Express urgency to them for the next 2-4 weeks. OPTION for this Council Point person to stay on as a liaison or a new liaison could be chosen. 


QUALIFICATIONS and success criteria:

- Be able to work on writing the scope in the next couple weeks


Nomination round 1: 
- HPY: JS (because he thinks a lot about the GOSH Gathering processes and how to make them better)
- AQ: Julian (because JS knows Julieta and Greg)
- LO: MF (because she has great communication skills and important!)
- MF: Julian (because JS has attended GOSH global, aware of different aspects/considerations of running event)
- EB: AQ  (because of experience with Dinacon to help Julieta and Greg to define scope)
- JS : Andy (because great experience with Dinacon which is a big event)

Nomination round 2 
- HPY AQ
- AQ Julian
- LO andy
- MF 
- EB : julian who seems like he really does in fact actually truly know Juli's scedule lol
JS Andy

**PROPOSAL:** Julian, because of schedule and connectivity to key players

**TERM:** review in four weeks

(PS, the person nominated has the opportunity to decline! and then facilitator has to choose another)


**DECISION:* YES!!!!!!!! We decided on Julian to reach out to Julieta and Greg

**OBJECTIONS ROUND:** say "no objections" or state your objection

NO OBJECTIONS!
