# Role selection for GOSH nonprofit board

## NEED for the role(s): 

- increase transparency of GOSH Inc operations because they hold so much money
- Foster flow of information between GOSH Inc and Community Council?
- Be able to stay in the role for the next 2 years (2-year members: Julian, Laura, Pierre, Pen) - this was requested and strongly preferred by the NGO but we can send whoever we want. 
- Be legally allowed to fulfill this role (we discussed with jenny and it seems we all should qualify!)
- be an adminstrative link
- be an interface that can check for alignment between two groups - perhaps even more important than actual hardware and nonprofit admin skills

**SYNTHESIZED NEEDS STATEMENT:** (check for internal contradictions in the above)


## SCOPE of the role(s):

- have name appear on legal documents of GOSH Inc.
- attend quarterly board meetings
- obtain agenda of board meetings in advance and share with Council so that any items may be discussed in order with the results that the delegate is equipped with the best information and perspectives
- Represent the voices of the council when making official decisions with GOSH inc.
- check that actions align with the mission and money/accounting is done well "due diligence"
- maintain responsibilities to the funder (SD and JM will handle basically all of this)

**SYNTHESIZED SCOPE:** (check for any internal contradictions?)  
Would not like to be in this role: Pierre, Liz, Maria and Laura


## QUALIFICATIONS and success criteria: (HERE'S WHERE TO START NEXT WEEK)
- EB: someone who is quick at processing verbally delivered information
- MF: someone who has ability to contribute a different perspective than the members of the group they are sitting with (different perspective as to Jenny and Shannon) - just having a more diverse group. +1 EB
- MF: ability to fulfill the Time commitment too!
- HPY: attend all the board meetings (and CC meetings too). Having experience working with board members might be useful
-JS: Mandate from the community, recognize that we [this seat would be] are only community elected person on the board.
 - AQ: What is meant by mandate?
 - JS: Looked at star.vote and JS was condorcet winner
- PP: pass
- AQ: pass
- LO: Person should have some administration and/or accounting experience


## SYNTHESIZED QUALS AND CRITERIA


**Nomination round 1:**

- HPY - JS - would help things get started fast
- AQ - Pen- diversity in "passports"
- PP - Pen - based on Pen's reflection on the voting process and his ability to share information helpfully
- LO - AQ - good verbal summarization of information and sharing of information among group. Experience of organizing own lab globally. Like all candidates. 
- MF - Pen - to create a more diverse group
- EB - pierre - based on criteria of bringing the most locally connected viewpoints to the global board. 
- JS - JS- condorcet winner has the most backing, that is me. I do accounting for my own business, i can do this. 

**Nomination round 2**

- HPY - JS - moved by reason LO voted for AQ, however maintained nomination because you don't often hear someone say upfront that they want to do accounting work
- AQ -Julian (JS) - switched to JS because of his enthusiasm towards accounting work and his relationship with board is good and role seems like more of a clerical job
- PP -  pEN - maintained nomination for reason of diverse representation
LO- JS - switched to JS because he convinced me! His self-confidence in assuming the role which is very important because there will be a lot of tasks to solve and meetings to attend, so need that conviction from the beginning
- MF - Pen - maintained nomination on how langauge shapes the world and our perspectives, would almost always go for perspective over the harder scales like accounting, when deciding how funding gets distributed is when the importance of perspectives comes in. Well-rounded, hitting from many angles. 
- EB - pen - switched nomination to HPY because of the precision of which he communicates verbally and in textual documents means he is a great link between board and CC,  his critical thinking is fast, adds a greater sense of global diversity to the board
- JS - JS - pass

## PROPOSAL: 
Julian Stirling is nominated 



**DECISION:**

**OBJECTIONS ROUND:** say "no objections" or state your objection

- HPY - no objections
- AQ - no objections
- PP - no objections
- LO - no objections
- MF - no objections
- EB - no objections
- JS - no objections

