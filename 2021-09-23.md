# GOSH Community Council Meeting 2021-09-23 for 90 minutes

## Present: (asynchronously type your name and initials for use in the notes below)

* Bri 
* lizbarry (EB)
* Pen-Yuan Hsing (HPY)
* Pierre (PP)
* Laura (LO)
* Julian (JS)

## Check-in warm up activity: What is a math or science question you kinda understand but still find hard to grasp? 

## Health Check: updates and questions only, no discussion. Let's recap all news shared since last meeting: 

*Time 0:10 - 0:20*

**Working Group and Community Manager health checks: **

* Funding WG (PP + EB): sketch of timeline for GOSH Collaborative Development Program: [here](working-pads/GOSH-funding-distro-role-selection.html) (also in agenda?)
    * Saturday confusion!
    * Tuesday worksession :)
* Gathering WG (JS, AQ, MF, LO): 
    * AQ: Gathering Working Group Update (Schedule, letter)
        * CURRENT PLAN: Gathering invite Letter [https://docs.google.com/document/d/1hH0aNFOWEsskVqE3DrPytcJMc7rBhjqfihMCQuSgU2o/edit](https://docs.google.com/document/d/1hH0aNFOWEsskVqE3DrPytcJMc7rBhjqfihMCQuSgU2o/edit)
            * includes some compensation (flights + hotel stay)
            * created two special spots for AfricaOSH person and ReGOSH person
        * Sloan extension letter with placeholder dates of November 1-4 [https://docs.google.com/document/d/1oPvqJpc4Ke4SEwE40NQELPyBlH5gXCOcs4lH2FPHN\_w/edit](https://docs.google.com/document/d/1oPvqJpc4Ke4SEwE40NQELPyBlH5gXCOcs4lH2FPHN\_w/edit)
        * Cryptpad form [https://cryptpad.fr/form/#/2/form/view/1qkLp7OMt544qRtZPoaUaFa-DjL-Nj43+h+l-pxldEc/](https://cryptpad.fr/form/#/2/form/view/1qkLp7OMt544qRtZPoaUaFa-DjL-Nj43+h+l-pxldEc/)
            * More folks from AfricOSH, and Greg and someone from Greg's Lab. 
* Board Representative Julian Stirling: any updates? 
    * SD sent JS a message that the next meeting would be in the 3rd or 4th week of September when they will vote Julian on and get in touch

**Council member health checks: **

   * HPY: Thanks for the GOSH community's support for my open science online course, the beta testing ("soup tasting") is starting now-ish: [https://forum.openhardware.science/t/seeking-testers-for-open-science-learning-module/3094?u=hpy](https://forum.openhardware.science/t/seeking-testers-for-open-science-learning-module/3094?u=hpy)
       * Since this course is open source (CC BY 4.0), if there's interest from the GOSH community, maybe we could use it as a test case for the putative knowledge archiving/organisation WG?????
   * HPY: worked on list of our facilitation processes, looking for feedback on (1) its contents, (2) thoughts on the next step
       * [https://md.opensourceecology.de/s/Z1uFH_Llk#](https://md.opensourceecology.de/s/Z1uFH_Llk#)
       * Results of our reflection could also be a test case for PUTATIVE knowledge archiving/organisation WG
   * EB: did not get to drafting policy for independent working group formation -- request another week?
   * EB: we need to discuss what we (the council) might share at the Community Call on Wednesday September 29 as per Bri's invitation

## Review Proposed Agenda

*Time 0:20 - 0:28*

* PP + EB present the grant scope for "GOSH Collaborative Development Program", and the Council kicks it around a bit
* we need to discuss what we (the council) might share at the Community Call on Wednesday September 29

### Review possible agenda items for next week

   * HPY: facilitation reflection
   * ALL: do we need to do anything considering this will be the last meeting in September before point people have to report to the Board on Gathering and Funding timelines???
   * JS:  policy for independent working group formation

## Agenda Topic 1 : Report back from Funding Distribution WG Pierre and Liz

*Time 0:30 - 0:50 *

CONTEXT: [here](working-pads/GOSH-funding-distro-role-selection.html)

Asking more than 6 month extention - up to 18 months. None of the 110,000 dollars spent yet and this is a huge program. 

JS: use hardware-specific licenses, ask Andrew Katz

$110,000 to spend, PP proposing 70% go to well known but 30% go to new, need to build program of extended collaboration

Spring - Summer - Fall session (3 sessions per year), 2 with advanced and 1 with new. All three collab with professionals built in. December serves as review period

Program coordinator for this?

MF: Money will be more well-spent for concentrated in-person worksessions +1 JS, +1 AQ, +1 HPY 
    * HPY: should ask how Open Source Ecology's remote events have been going

HPY: how will flagship projects be identified? (perhaps the project needs a flag?) lol

HPY: make sure applications aren't rejected because the reviewers do not understand

HPY: See if we can get OScH from fields/disciplines not well-represented in GOSH represented.

JS: new projects...there's an open source trap of making 30 of the same version...what if grants come in from 4 groups all wanting to do the same project? Can these groups work together? (AQ: "collaborate and conquer"!)

EB: mentioning that advanced projects need to get to the next step, and that there might be a justification for funding new projects 

MF: the application should have a roadmap template to really make people think through how to get from where they are to where they want to be, and how these other professionals are needed. Emphasize the plan, and that dates matter. One template for a new project, a different one for advanced projects. 

JS: the starting point for many people from scientific and technical background is "make the thing we need". it's powerful to consider bringing in experts in manufacturing. They asked for a "Failure Plan Analysis" -- we didn't have this. We need people to really articulate what phase of development they are in, so we know what type of experience we need to bring in as consultant. The original project team doesn't know what they dont know. 

   * Possible to have a 2-phase grant. Some number of projects (and experts) get a first grant to write a detailed roadmap, and work out what needs to be done to meet a specific goal. This roadmap forms the second phase application. A smaller number would then get further funding to implement the roadmap.
MF: think about the Minimum Viable Product. The project team DOES need to make their plan and think about their time allocation. 

HPY: if we can figure this out, then the "Hardware Carpentry outcome" would be Very Valuable because it will live far beyond the life of the grant, and maybe make for a stronger case for grant extension?

LO: an external curator can review the project at different phases, ending in another result which is different from what was originally proposed, but is also good. 

## Agenda Topic 2: What council can share at Community call

*Time 0:50 - 1:20*

## CLOSING ROUND: What are you taking with you as you depart? what do you need to say to leave this meeting with integrity? 

*Time 1:20 - 1:30*
